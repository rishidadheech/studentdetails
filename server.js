const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// connection configurations
const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'studentportal'
});

// connect to database
mc.connect();

// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});

// Retrieve all student details
app.get('/getData', function (req, res) {
    mc.query('SELECT * FROM studentdetails', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'get data list.' });
    });
});

// Retrieve student details with name
app.get('/getData/:name', function (req, res) {

    let name = req.params.name;

    if (!name) {
        return res.status(400).send({ error: true, message: 'Please provide namme' });
    }

    mc.query('SELECT * FROM studentdetails where name=?', name, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'student list.' });
    });

});

// Add a new student
app.post('/addNew', function (req, res) {

    let name = req.body.name;
    let address =req.body.address;
    let mobile = req.body.mobile;
    let email =req.body.email;

    if (!name) {
        return res.status(400).send({ error:true, message: 'Please provide details' });
    }

    mc.query("INSERT INTO studentdetails SET ?", {name:name,address:address,mobile:mobile,email:email} , function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New student has been created successfully.' });
    });
});

//  Delete student
app.delete('/deleteStudent', function (req, res) {

    let name = req.body.name;

    if (!name) {
        return res.status(400).send({ error: true, message: 'Please provide task_id' });
    }
    mc.query('DELETE FROM studentdetails WHERE name = ?', [name], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'student has been deleted successfully.' });
    });
});

// port must be set to 8080 because incoming http requests are routed from port 80 to port 8080
app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});
